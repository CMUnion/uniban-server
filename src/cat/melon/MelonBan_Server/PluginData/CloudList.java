package cat.melon.MelonBan_Server.PluginData;

import java.util.ArrayList;
import java.util.List;

public class CloudList {
    private int UID;
    List<BannedPlayer> players ;
	
	public CloudList() {
	}

    public CloudList(int UID, ArrayList<BannedPlayer> players) {
        this.UID = UID;
        this.players=players;
    }

    public int getUID() {
        return UID;
    }

    public List<BannedPlayer> getPlayers() {
        return players;
    }
}
