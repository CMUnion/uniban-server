package cat.melon.MelonBan_Server.PluginData;

import java.util.Objects;
import java.util.Random;
import java.util.UUID;

public class BannedPlayer{
    public UUID uuid;
    public long time;
    public String reason;

    public BannedPlayer() {
    }

    /**
     * debug method
     *
     * @return random BannedPlayer
     */
    @Deprecated
    public static BannedPlayer randomPlayer() {
        Random random = new Random();
        int i = random.nextInt(0xffffff);
        return new BannedPlayer(UUID.randomUUID(),i,"A random player");
    }

    public BannedPlayer(UUID uuid, long time, String reason) {
        this.uuid = uuid;
        this.time = time;
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BannedPlayer that = (BannedPlayer) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
