package cat.melon.MelonBan_Server.Packets;

import cat.melon.MelonBan_Server.Utils.AESEnctyper;

import java.util.HashMap;
import java.util.Map;

public class ServerNamePacket extends PacketIdentify implements Packet{
    Map<Integer,String> servernames = new HashMap<>();

    public ServerNamePacket(int UID, AESEnctyper enctyper, Map<Integer, String> servername) {
            super(PacketType.SERVER_NAME_PACKET, UID, enctyper);
        this.servernames = servername;
    }

    public ServerNamePacket() {}

    public Map<Integer, String> getServernames() {
        return servernames;
    }
}
