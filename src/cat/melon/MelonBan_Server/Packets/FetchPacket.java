package cat.melon.MelonBan_Server.Packets;

import cat.melon.MelonBan_Server.Utils.AESEnctyper;

import java.util.List;

public class FetchPacket extends PacketIdentify implements Packet {
    List<Integer> lists ;

    public FetchPacket(int UID, AESEnctyper enctyper, List<Integer> lists) {
        super(PacketType.FETCH_LIST, UID, enctyper);
        this.lists = lists;
    }
}
