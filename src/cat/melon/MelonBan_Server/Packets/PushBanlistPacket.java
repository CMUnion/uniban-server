package cat.melon.MelonBan_Server.Packets;

import cat.melon.MelonBan_Server.PluginData.BannedPlayer;
import cat.melon.MelonBan_Server.Utils.AESEnctyper;

import java.util.List;

public class PushBanlistPacket extends PacketIdentify implements Packet {
    List<BannedPlayer> list;

    public PushBanlistPacket(int UID, AESEnctyper enctyper, List<BannedPlayer> banlist) {
        super(PacketType.PUSH_LIST, UID, enctyper);
        this.list = banlist;
    }

    public PushBanlistPacket() {}

    public List<BannedPlayer> getList() {
        return list;
    }
}
