package cat.melon.MelonBan_Server.Packets;

import cat.melon.MelonBan_Server.Utils.AESEnctyper;

public class ReturnPacket extends PacketIdentify implements Packet {
    public int status;

    public ReturnPacket(int UID, AESEnctyper enctyper, int status){
        super(PacketType.RETURN, UID, enctyper);
        this.status = status;
    }

    public ReturnPacket() {}

    public int getStatus(){
        return status;
    }
}
